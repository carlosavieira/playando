# Playando

Uma aplicação web que adicione vídeos do YouTube a uma lista, usando os dados da API do YouTube. Faz parte de um teste técnico para a empresa **[Pelando](https://www.pelando.com.br/)**.

## Para iniciar

Primeiro é necessário instalar as dependências com:

 ```bash
npm ci
 ```

Para executar a aplicação, basta fazer:

 ```bash
npm run dev
 ```

ou para fazer o build:

 ```bash
npm run build
 ```

Note que é necessário definir em um arquivo `.env` a API key do YouTube.

Obs.: Ao enviar os arquivos para o teste, envio junto o arquivo `.env.local` devidamente preenchido com a API key. Esse arquivo só não é enviado para o Git.

### Detalhe Importante

Por conta de restrições da minha API key, para as chamadas a API funcionarem é necessário que o aplicação esteja em algum dos endereços:

- **localhost:3000**
- **localhost:4000**
- **localhost:8080**

## Detalhes do desafio

### Requisitos

- Se houver uma lista armazenada, ela deve ser restaurada.
- O usuário pode digitar uma url do YouTube ou o título de um vídeo a ser buscado.
- Se o usuário digitar uma url do YouTube, o botão deve exibir o texto “adicionar”, caso contrário, deve exibir “buscar”.
- Se o usuário clicar em “buscar”, um modal deve ser aberto com uma lista de 4 vídeos para que um possa ser escolhido.
- Ao adicionar um vídeo, ele deve aparecer no final da playlist com título e thumbnail obtidos pela API do YouTube.
- Os vídeos da lista podem ser filtrados por palavras-chave encontradas nos títulos. Isto significa que “criando promo” dá match em “Criando sua primeira promoção”. A ordem das palavras-chave no título deve ser respeitada.
- Ao clicar em filtrar, o botão “filtrar” muda para “limpar filtro”. Clicando no mesmo botão, a lista volta ao estado inicial.
- Ao clicar no botão de play sobre o vídeo, o estado do botão deve mudar e o vídeo pode ser pausado clicando no mesmo botão.
- Se um vídeo estiver tocando e o usuário clicar no botão de play de outro vídeo, o primeiro vídeo deve pausar.
- Ao clicar no “x” sobre o vídeo, o vídeo deve ser removido da lista.
- Os títulos dos vídeos devem ocupar no máximo 2 linhas e mostrar “...” no final da 2ª linha caso excedam.
- As thumbnails devem usar a proporção de 16:9 (ex.: 480x270, 320x180). Se a imagem utilizada não tiver essa proporção, ela deve ser cortada e centralizada.

### Desejável

- Aplicação em React
- Validação das entradas do usuário
- Tratamentos de casos de erro / falta de conteúdo
- Acessibilidade
- Animações e mudanças de estado nas interações
- Evitar requisições descartáveis ou repetidas
- Utilizar imagens de tamanhos ideais para cada dispositivo
