import { useEffect, useState } from "react";
import youtube, { VideoData } from "./apis/youtube";
import "./App.css";
import Filter from "./components/Filter";
import Modal from "./components/Modal";
import Search from "./components/Search";
import VideoList from "./components/VideoList";
import { VideoPlayingProvider } from "./context/VideoPlayingContext";

function App() {
  const [videoList, setVideoList] = useState<VideoData[]>(
    JSON.parse(localStorage.getItem("videoList") || "[]")
  );
  const [searchResults, setSearchResults] = useState<VideoData[]>([]);
  const [filter, setFilter] = useState("");

  const handleSearch = (text: string) => {
    youtube.searchVideos(text).then((newVideos) => {
      setSearchResults(newVideos.slice(0, 4));
    });
  };

  const handleGetAndAdd = (id: string) => {
    youtube.getVideo(id).then((newVideo) => {
      setVideoList([...videoList, newVideo]);
    });
  };

  const handleAddSelected = (newVideo: VideoData) => {
    setSearchResults([]);

    if (!newVideo) {
      return;
    }

    setVideoList([...videoList, newVideo]);
  };

  const handleFilter = (filter: string) => {
    setFilter(filter);
  };

  const handleRemove = (video: VideoData) => {
    setVideoList(
      videoList.filter(
        (v) => v.id !== video.id || v.fetchedAt !== video.fetchedAt
      )
    );
  };

  const handleModalClose = () => {
    document.getElementById("searchInput")?.focus();
    setSearchResults([]);
  };

  useEffect(() => {
    localStorage.setItem("videoList", JSON.stringify(videoList));
  }, [videoList]);

  return (
    <div className="App">
      <VideoPlayingProvider>
        <header>
          <h1 id="mainTitle" className="title">
            Playando
          </h1>
        </header>
        <main aria-labelledby="mainTitle">
          <section>
            <Search handleAdd={handleGetAndAdd} handleSearch={handleSearch} />
            <hr />
            <Filter handleFilter={handleFilter} />
            <div className="spacer"></div>
          </section>
          <section>
            <VideoList
              videoList={videoList}
              filter={filter}
              mode="play"
              handleRemove={handleRemove}
            />
          </section>
          {!!searchResults.length && (
            <Modal
              title="Adicionar vídeo"
              open={true}
              onClose={handleModalClose}
            >
              <VideoList
                mode="add"
                videoList={searchResults}
                handleAdd={handleAddSelected}
              ></VideoList>
            </Modal>
          )}
        </main>
      </VideoPlayingProvider>
    </div>
  );
}

export default App;
