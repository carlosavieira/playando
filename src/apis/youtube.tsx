const baseUrl = "https://www.googleapis.com/youtube/v3/";
const apiKey = import.meta.env.VITE_GOOGLE_API_KEY;
const headers = new Headers({
  Accept: "application/json",
});

interface SearchResult {
  items: {
    kind: string;
    etag: string;
    id: {
      kind: string;
      videoId: string;
    };
    snippet: Snippet;
  }[];
}

interface VideoResult {
  items: {
    kind: string;
    etag: string;
    id: string;
    snippet: Snippet;
  }[];
}

interface Snippet {
  publishedAt: string;
  channelId: string;
  title: string;
  description: string;
  thumbnails: Thumbnails;
  channelTitle: string;
  liveBroadcastContent: string;
  publishTime: string;
}

interface Thumbnails {
  default: Thumbnail;
  medium: Thumbnail;
  high: Thumbnail;
}

interface Thumbnail {
  url: string;
  width: number;
  height: number;
}

export interface VideoData {
  id: string;
  title: string;
  thumbnail: Thumbnail;
  fetchedAt: number;
}

export const getVideoKey = (video: VideoData) =>
  `${video.id}/${video.fetchedAt}`;

export const convertListToVideoData = (
  videos: SearchResult["items"]
): VideoData[] =>
  videos.map((video) => ({
    id: video.id.videoId,
    title: video.snippet.title,
    thumbnail: video.snippet.thumbnails.high,
    fetchedAt: Date.now(),
  }));

export const convertToVideoData = (videos: VideoResult["items"]): VideoData =>
  videos.map((video) => ({
    id: video.id,
    title: video.snippet.title,
    thumbnail: video.snippet.thumbnails.high,
    fetchedAt: Date.now(),
  }))?.[0];

export default {
  searchVideos: async (searchText: string) => {
    const url = `${baseUrl}search?part=id&part=snippet&q=${searchText}&type=video&key=${apiKey}`;

    const response = await fetch(url, { headers, method: "GET" });

    const data: SearchResult = await response.json();

    if (response.ok) {
      const videoList = data?.items;
      if (videoList?.length) {
        return convertListToVideoData(videoList);
      } else {
        return Promise.reject(
          new Error(`No video with the search "${searchText}"`)
        );
      }
    } else {
      const error = new Error(`API Error code: ${response.status}`);
      return Promise.reject(error);
    }
  },
  getVideo: async (id: string) => {
    const url = `${baseUrl}videos?part=%20id&part=contentDetails&part=player&part=snippet&part=statistics&id=${id}&key=${apiKey}`;

    const response = await fetch(url, { headers, method: "GET" });
    const data: VideoResult = await response.json();

    if (response.ok) {
      const videoList = data?.items;
      if (videoList?.length) {
        return convertToVideoData(videoList);
      } else {
        return Promise.reject(new Error(`No video with the id "${id}"`));
      }
    } else {
      const error = new Error(`API Error code: ${response.status}`);
      return Promise.reject(error);
    }
  },
};
