import { FormEvent, useState } from "react";
import { VideoData } from "../apis/youtube";
import "./Filter.css";

const replaceSpecialChars = (str: string) => {
  const normalized = str
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "") // Remove accents
    .replace(/([^\w]+)/g, " ") // Replace other characters by space
    .replace(/\s\s+/g, " "); // Replaces multiple spaces by one space

  return normalized.trim().toLocaleLowerCase();
};

export const filterFromList = (
  filter: string,
  videos: VideoData[]
): VideoData[] => {
  const filterList = replaceSpecialChars(filter).split(" ");

  return videos.filter((video) => {
    const videoTitleIterable = replaceSpecialChars(video.title)
      .split(" ")
      .entries();

    let ok = false;
    return !filterList.some((wordToLocate) => {
      let nextWord;

      do {
        nextWord = videoTitleIterable.next()?.value?.[1];

        if (!nextWord) {
          // if there are no more words to iterate
          return true;
        } else if (nextWord.includes(wordToLocate)) {
          // if the word is found
          return false;
        }
      } while (nextWord);
    });
  });
};

function Filter(props: { handleFilter: (filter: string) => void }) {
  const [filterInput, setFilterInput] = useState("");
  const [filterIsActive, setFilterIsActive] = useState(false);

  const handleChanges = (e: FormEvent<HTMLInputElement>) => {
    const newFilter = e.currentTarget.value;
    setFilterInput(newFilter);
  };

  const handleSubmit = async (event: FormEvent) => {
    event.preventDefault();

    console.log("updating filter to: " + filterInput);

    props.handleFilter(filterInput);
    setFilterIsActive(true);
  };

  const handleReset = async () => {
    console.log("resetting filter");
    props.handleFilter("");
    setFilterInput("");
    setFilterIsActive(false);
  };

  return (
    <div className="filterSpacer">
      <form
        className="filter"
        action="submit"
        onSubmit={handleSubmit}
        onReset={handleReset}
      >
        <label htmlFor="filterInput" aria-label="Filtro"></label>
        <input
          id="filterInput"
          type="text"
          placeholder=" palavras-chave"
          value={filterInput}
          onInput={handleChanges}
        />

        {!filterIsActive && (
          <button disabled={!filterInput?.trim().length} type="submit">
            filtrar
          </button>
        )}
        {filterIsActive && <button type="reset">limpar filtro</button>}
      </form>
    </div>
  );
}

export default Filter;
