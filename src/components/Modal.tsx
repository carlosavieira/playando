import { useEffect, useState } from "react";
import "./Modal.css";
import VideoButton from "./VideoButton";

type FocusableElement =
  | HTMLAnchorElement
  | HTMLButtonElement
  | HTMLInputElement
  | HTMLTextAreaElement
  | HTMLSelectElement;

function trapFocus(element: HTMLElement) {
  var focusableEls: FocusableElement[] = [
    ...element
      .querySelectorAll(
        'a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled])'
      )
      .values(),
  ] as FocusableElement[];

  var firstFocusableEl = focusableEls[0];
  var lastFocusableEl = focusableEls[focusableEls.length - 1];
  var KEYCODE_TAB = 9;

  element.addEventListener("keydown", function (e) {
    var isTabPressed = e.key === "Tab" || e.keyCode === KEYCODE_TAB;

    if (!isTabPressed) {
      return;
    }

    if (e.shiftKey) {
      /* shift + tab */ if (document.activeElement === firstFocusableEl) {
        lastFocusableEl.focus();
        e.preventDefault();
      }
    } /* tab */ else {
      if (document.activeElement === lastFocusableEl) {
        firstFocusableEl.focus();
        e.preventDefault();
      }
    }
  });
}

function Modal(props: {
  children: React.ReactNode;
  title: string;
  open: boolean;
  onClose: () => void;
}) {
  const [isOpen, setIsOpen] = useState(props.open);

  const handleClose = () => {
    setIsOpen(false);
    props.onClose();
  };

  useEffect(() => {
    document.getElementById("modalTitle")?.focus();

    const dialogElement = document.getElementById("modal") as HTMLElement;
    trapFocus(dialogElement);

    function handleEscapeKey(event: KeyboardEvent) {
      if (event.code === "Escape") {
        handleClose();
      }
    }

    document.addEventListener("keydown", handleEscapeKey);
    return () => document.removeEventListener("keydown", handleEscapeKey);
  }, []);

  return (
    <dialog
      id="modal"
      role="dialog"
      aria-modal="true"
      aria-labelledby="modalTitle"
      open={isOpen}
      tabIndex={-1}
    >
      <div className="modalContainer">
        <header className="header">
          <h1 id="modalTitle" tabIndex={-1}>
            {props.title}
          </h1>
          <VideoButton
            title="Fechar modal"
            type="delete"
            onClick={handleClose}
          ></VideoButton>
        </header>
        <main className="content">{props.children}</main>
      </div>
    </dialog>
  );
}

export default Modal;
