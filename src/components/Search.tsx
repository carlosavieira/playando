import { FormEvent, useState } from "react";
import "./Search.css";

const youtubeUrlRegExp =
  /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)/;

type InputState = "EMPTY" | "YOUTUBE-URL" | "SEARCH-TERM";
type SubmitButtonText = "adicionar" | "buscar";

function Search(props: {
  handleSearch: (text: string) => void;
  handleAdd: (id: string) => void;
}) {
  const [text, setText] = useState("");
  const [submitButtonText, setSubmitButtonText] =
    useState<SubmitButtonText>("buscar");
  const [inputState, setInputState] = useState<InputState>("EMPTY");

  const handleChanges = (e: FormEvent<HTMLInputElement>) => {
    const newText = e.currentTarget.value;
    setText(newText);
    const newInputType = checkInputType(newText?.trim());

    if (newInputType === "YOUTUBE-URL") {
      setSubmitButtonText("adicionar");
    } else if (newInputType === "SEARCH-TERM") {
      setSubmitButtonText("buscar");
    }
    setInputState(newInputType);
  };

  const checkInputType = (input: string) => {
    const value = input?.trim();
    if (!value?.length) {
      return "EMPTY";
    } else if (value.match(youtubeUrlRegExp)?.[5]) {
      return "YOUTUBE-URL";
    } else {
      return "SEARCH-TERM";
    }
  };

  const handleSubmit = async (event: FormEvent) => {
    event.preventDefault();
    const value = text?.trim();

    if (inputState === "YOUTUBE-URL") {
      props.handleAdd(value.match(youtubeUrlRegExp)?.[5] ?? "");
      setText("");
    } else if (inputState === "SEARCH-TERM") {
      props.handleSearch(value);
      setText("");
    }
  };

  return (
    <div className="searchSpacer">
      <form className="search" action="submit" onSubmit={handleSubmit}>
        <label htmlFor="searchInput" aria-label="Campo de pesquisa"></label>
        <input
          id="searchInput"
          type="text"
          value={text}
          placeholder=" link ou título do vídeo"
          onInput={handleChanges}
          autoFocus={true}
        />

        <button id="searchButton" type="submit">
          {submitButtonText}
        </button>
      </form>
    </div>
  );
}

export default Search;
