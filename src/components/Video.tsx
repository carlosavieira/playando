import { useContext, useEffect, useState } from "react";
import { getVideoKey, VideoData } from "../apis/youtube";
import { VideoPlayingContext } from "../context/VideoPlayingContext";
import "./Video.css";
import VideoButton from "./VideoButton";

type VideoState = "playing" | "paused" | "stopped";

function Video(props: {
  item: VideoData;
  order: number;
  mode: "play" | "add";
  handleAdd?: () => void;
  handleRemove?: () => void;
}) {
  const { playId, isPlayingId } = useContext(VideoPlayingContext);
  const [currently, setCurrently] = useState<VideoState>("stopped");
  const iframeId = `video-${getVideoKey(props.item)}`;

  const videoCommand = (func: "playVideo" | "stopVideo" | "pauseVideo") => {
    const iframe: HTMLIFrameElement = document.getElementById(
      iframeId
    ) as HTMLIFrameElement;

    iframe.contentWindow?.postMessage(
      JSON.stringify({ event: "command", func }),
      "*"
    );
  };

  const playVideo = () => {
    setCurrently("playing");
    videoCommand("playVideo");
    playId(getVideoKey(props.item));
  };

  const pauseVideo = () => {
    setCurrently("paused");
    videoCommand("pauseVideo");
  };

  useEffect(() => {
    if (isPlayingId !== getVideoKey(props.item) && currently === "playing") {
      pauseVideo();
    }
  }, [isPlayingId]);

  return (
    <article className="cardContainer">
      <div className="videoContainer">
        <img
          className="thumbnail"
          alt="Imagem miniatura do vídeo"
          style={{ display: currently != "stopped" ? "none" : "block" }}
          src={props.item.thumbnail.url}
        ></img>
        <iframe
          className="video"
          title={props.item.title}
          id={iframeId}
          src={`https://www.youtube.com/embed/${props.item.id}?modestbranding=1&autohide=1&showinfo=0&enablejsapi=1`}
          style={{ border: 0 }}
          allow="autoplay; encrypted-media"
          allowFullScreen
        />{" "}
      </div>

      <div className="controls">
        <div className="leftControl">
          {currently != "playing" ? (
            <VideoButton onClick={playVideo} title="Tocar vídeo" type="play" />
          ) : (
            <VideoButton
              onClick={pauseVideo}
              title="Pausar vídeo"
              type="pause"
            />
          )}
        </div>

        <div className="rightControl">
          {props.mode === "add" ? (
            <VideoButton
              onClick={props.handleAdd}
              title="Adicionar vídeo"
              type="add"
            />
          ) : (
            <VideoButton
              onClick={props.handleRemove}
              title="Remover vídeo"
              type="delete"
            />
          )}
        </div>
      </div>
      <div className="title">
        <p title={props.item.title}>
          <strong>{props.order}. </strong>
          {props.item.title}
        </p>
      </div>
    </article>
  );
}

export default Video;
