import { useMemo, useState } from "react";
import { ReactComponent as Add } from "./icons/add.svg";
import { ReactComponent as Add2 } from "./icons/add_selected.svg";
import { ReactComponent as Delete } from "./icons/delete.svg";
import { ReactComponent as Delete2 } from "./icons/delete_selected.svg";
import { ReactComponent as Pause } from "./icons/pause.svg";
import { ReactComponent as Pause2 } from "./icons/pause_selected.svg";
import { ReactComponent as Play } from "./icons/play.svg";
import { ReactComponent as Play2 } from "./icons/play_selected.svg";

export function VideoButton(props: {
  type: "play" | "pause" | "add" | "delete";
  title: string;
  ariaLabel?: string;
  onClick?: () => void;
}) {
  const [selected, setSelected] = useState(false);

  const svg = useMemo(() => {
    switch (props.type) {
      case "play":
        return !selected ? <Play /> : <Play2 />;
      case "pause":
        return !selected ? <Pause /> : <Pause2 />;
      case "add":
        return !selected ? <Add /> : <Add2 />;
      case "delete":
        return !selected ? <Delete /> : <Delete2 />;
      default:
        return null;
    }
  }, [props.type, selected]);

  const handleOnClick = () => {
    setSelected(false);
    return props.onClick?.();
  };

  return (
    <>
      <button
        aria-label={props.ariaLabel}
        title={props.title}
        onPointerEnter={() => setSelected(true)}
        onPointerLeave={() => setSelected(false)}
        onClick={handleOnClick}
      >
        {svg}
      </button>
    </>
  );
}

export default VideoButton;
