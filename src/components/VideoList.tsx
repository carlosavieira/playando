import { useMemo } from "react";
import { getVideoKey, VideoData } from "../apis/youtube";
import { filterFromList } from "./Filter";
import Video from "./Video";
import "./VideoList.css";

export function VideoList(props: {
  videoList: VideoData[];
  filter?: string;
  mode: "play" | "add";
  handleAdd?: (video: VideoData) => void;
  handleRemove?: (video: VideoData) => void;
}) {
  const filteredVideoList = useMemo(
    () =>
      props.filter
        ? filterFromList(props?.filter, props.videoList)
        : props.videoList,
    [props.filter, props.videoList]
  );

  return (
    <>
      <div className="videoList">
        {filteredVideoList.map((item) => (
          <Video
            order={1 + props.videoList.indexOf(item)}
            key={getVideoKey(item)}
            item={item}
            mode={props.mode}
            handleAdd={() => props.handleAdd?.(item)}
            handleRemove={() => props.handleRemove?.(item)}
          />
        ))}
      </div>
      {filteredVideoList.length != props.videoList.length && (
        <p>
          {filteredVideoList.length}/{props.videoList.length} videos filtrados.
        </p>
      )}
    </>
  );
}

export default VideoList;
