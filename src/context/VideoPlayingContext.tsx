import { createContext, useState } from "react";

export const VideoPlayingContext = createContext({
  isPlayingId: "",
  playId: (id: string) => {},
});

export const VideoPlayingProvider = (props: { children: any }) => {
  const [videoPlaying, setVideoPlaying] = useState("");

  return (
    <VideoPlayingContext.Provider
      value={{ isPlayingId: videoPlaying, playId: setVideoPlaying }}
    >
      {props.children}
    </VideoPlayingContext.Provider>
  );
};
